package fileUploader;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


@WebServlet("/FileUploader")
@MultipartConfig
public class FileUploader extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public FileUploader() {
        super();
        
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		
        PrintWriter out = response.getWriter();
//        System.out.println(request.getParameter("file"));
//        System.out.println(request.getParameter("overwrite"));
	    Part filePart = request.getPart("file");
	    
	    String fileName = filePart.getSubmittedFileName();
        String path = "C:\\Users\\aayush.soni\\eclipse-workspace\\ServletAssignment\\src\\main\\webapp\\WEB-INF\\uploaded\\"+fileName;
        
        
        File file = new File(path);
        
//        if(file.exists()) {System.out.print("already ex");}
//        else {
//        	for(Part part:request.getParts()) {
//			part.write("C:\\Users\\aayush.soni\\eclipse-workspace\\ServletAssignment\\src\\main\\webapp\\WEB-INF\\uploaded\\"+fileName);
//		}
//        	System.out.print("uploaded");
//        }
        if(file.exists()) {
        	if(request.getParameter("overwrite").equals("yes")){
        		file.delete();
        		for(Part part:request.getParts()) {
        			part.write("C:\\Users\\aayush.soni\\eclipse-workspace\\ServletAssignment\\src\\main\\webapp\\WEB-INF\\uploaded\\"+fileName);
        		}
        		out.println("Success : file is overwritten");
        	}else {
        		out.println("OOPS! File already exists");
        	}
        }else {
        	for(Part part:request.getParts()) {
    			part.write("C:\\Users\\aayush.soni\\eclipse-workspace\\ServletAssignment\\src\\main\\webapp\\WEB-INF\\uploaded\\"+fileName);	
    		}
        	out.println("File Written Successfully");
       }
		
	}

}
