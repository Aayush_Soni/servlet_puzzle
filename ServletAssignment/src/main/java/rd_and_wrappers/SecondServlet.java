package rd_and_wrappers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class SecondServlet extends HttpServlet 
{
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		int k = (int) req.getAttribute("k");
		PrintWriter out = res.getWriter();
		out.println("cube of the addition of these numbers is " + k*k*k );
	}

}
