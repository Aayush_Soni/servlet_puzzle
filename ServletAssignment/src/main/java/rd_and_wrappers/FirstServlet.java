package rd_and_wrappers;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class FirstServlet extends HttpServlet 
{
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
	{
		int i = Integer.parseInt(req.getParameter("first_num"));
		int j = Integer.parseInt(req.getParameter("second_num"));
		int k = i+j;
		req.setAttribute("k", k);
		RequestDispatcher rd =req.getRequestDispatcher("cube");
		rd.forward(req, res);
	}

}
