package com.aayush0;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthServlet extends HttpServlet
{
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException 
	{
		Map<String,String> hm = new HashMap<>(); 
		hm.put("Aayush", "aayu13");
		hm.put("Shridhar", "shridhar");
		hm.put("Lekhana", "lekhana");
		hm.put("Suneel", "suneel");
		PrintWriter out = res.getWriter();
		
		String nm = req.getParameter("name");
		String pswd = req.getParameter("password");
		if(!hm.containsKey(nm)) out.println("oops the name doesnt exist");
		else {
			if(pswd.equals(hm.get(nm))) out.println("Name/Password matched");
			else out.println("Name/Password mismatched");
		}
		
		
		
	}
}
