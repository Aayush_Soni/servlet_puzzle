package httpClient;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/GetPostPutDelete")
public class HttpClientPostGetPutDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public HttpClientPostGetPutDelete() {
        super();       
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String nm = request.getParameter("name");
		out.println("Welcome to the 'GET' req "+ nm);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String nm = request.getParameter("name");
		out.println("Welcome to the 'POST' req "+ nm);
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String nm = request.getParameter("name");
		out.println("Welcome to the 'PUT' req "+ nm);
	}

	
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String nm = request.getParameter("name");
		out.println("Welcome to the 'DELETE' req "+ nm);
	}

}
